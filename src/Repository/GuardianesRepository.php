<?php

namespace App\Repository;

use App\Entity\Guardianes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Guardianes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Guardianes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Guardianes[]    findAll()
 * @method Guardianes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GuardianesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Guardianes::class);
    }

     /**
     * @return Guardianes[]
     */
    public function findByLastThree()
    {
        return $this->createQueryBuilder('g')
            ->orderBy('g.id', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult()
            ;
    }


    // /**
    //  * @return Guardianes[] Returns an array of Guardianes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Guardianes
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
