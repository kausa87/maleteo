<?php


namespace App\Form;

use App\Entity\DemoRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DemoFormType extends AbstractType
{
public function buildForm(FormBuilderInterface $builder, array $options)
{
    $builder->add("nombre");
    $builder->add("email");
    $builder->add("ciudad");
    $builder->add('save', SubmitType::class, [
        'attr' => ['class' => 'save']]);

}

public function configureOptions(OptionsResolver $resolver)
{
    $resolver->setDefaults([
        'data_class' => DemoRequest::class
    ]);
}
}