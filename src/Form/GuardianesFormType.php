<?php


namespace App\Form;

use App\Entity\Guardianes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class GuardianesFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
{
    $builder->add("comentario");
    $builder->add("autor");
    $builder->add("ciudad");
    $builder->add('save', SubmitType::class, [
        'attr' => ['class' => 'save']]);

}

    public function configureOptions(OptionsResolver $resolver)
{
    $resolver->setDefaults([
        'data_class' => Guardianes::class
    ]);
}
}