<?php


namespace App\Controller;

use App\Entity\DemoRequest;
use App\Entity\Guardianes;
use App\Entity\User;
use App\Form\DemoFormType;
use App\Form\GuardianesFormType;
use App\Form\UserFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class MainController extends AbstractController
{
    /**
     * @Route("/maleteo", name="homepage")
     */
    public function index()
    {
        return $this->render('base.html.twig',[]);
    }

    /**
     * @Route("/register", name="register")
     */
    public function adduser(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {

            $form = $this->createForm(UserFormType::class);
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $user = $form->getData();
                $user->setRoles(['USER_ROLE']);
                $password = $user->getPassword();
                $user->setPassword( $encoder->encodePassword($user, $password));
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute("homepage") ;
            }
            return $this->render(
                "register.html.twig",
                ["formulario" => $form->createView()]);
    }

    /**
     * @Route("/maleteo/solicitudes", name="listdemo")
     */
    public function listdemo(EntityManagerInterface $em){

        $repositorio = $em->getRepository(DemoRequest::class);
        $myreq = $repositorio->findAll();
        return $this->render('listdemo.html.twig',['auxdemos' => $myreq]);
}
    /**
     * @Route("/adddemo", name="adddemo")
     */
    public function adddemoreq( EntityManagerInterface $em)
    {
        $demo = new DemoRequest();
        $demo->setEmail("andrea@andrea.it");
        $demo->setCiudad("Roms");
        $demo->setNombre("Andrea manca");

        $em->persist($demo);
        $em->flush();
        $demo1 = new DemoRequest();
        $demo1->setEmail("andrea@andrea.es");
        $demo1->setCiudad("Madrid");
        $demo1->setNombre("Andrea manca");

        $em->persist($demo1);
        $em->flush();

        $demo2 = new DemoRequest();
        $demo2->setEmail("andrea@andrea.fr");
        $demo2->setCiudad("Paris");
        $demo2->setNombre("Andrea manca");

        $em->persist($demo2);
        $em->flush();

        return new Response("addeok ok");
    }

    /**
     * @Route("/maleteo/nuevademo", name="nuevademo")
     */
    public function nuevademo(Request $request, EntityManagerInterface $em){
        $form = $this->createForm(DemoFormType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $demo = $form->getData();
            $em->persist($demo);
            $em->flush();
             return $this->redirectToRoute("listdemo") ;
        }
        return $this->render(
            "nuevademo.html.twig",
            ["formulario" => $form->createView()]);
    }

    /**
     * @Route("/maleteo/listguardianes", name="listguardianes")
     */
    public function listguardianes(EntityManagerInterface $em){

        $repositorio = $em->getRepository(Guardianes::class);
        $myreq = $repositorio->findByLastThree();//$repositorio->findAll();
        return $this->render('guardianes.html.twig',['guardianes' => $myreq]);
    }

    /**
     * @Route("/maleteo/slider", name="slider")
     */
    public function slider(EntityManagerInterface $em){

        $repositorio = $em->getRepository(Guardianes::class);
        $myreq = $repositorio->findAll();
        return $this->render('slider.html.twig',['guardianes' => $myreq]);
    }

    /**
     * @Route("/maleteo/nuevoguardian", name="nuevoguardian")
     */
    public function nuevoguardian(Request $request, EntityManagerInterface $em){
        $form = $this->createForm(GuardianesFormType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $guardian = $form->getData();
            $em->persist($guardian);
            $em->flush();
            return $this->redirectToRoute("listguardianes") ;
        }
        return $this->render(
            "nuevoguardianes.html.twig",
            ["formulario" => $form->createView()]);
    }

    /**
     * @Route("/maleteo/editguardian/{id}", name="editguardian")
     */
    public function editguardian(Guardianes $guard, Request $request, EntityManagerInterface $em){


        $form = $this->createForm(GuardianesFormType::class, $guard);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $guardian = $form->getData();
            $em->persist($guardian);
            $em->flush();
            return $this->redirectToRoute("listguardianes") ;
        }
        return $this->render(
            "nuevoguardianes.html.twig",
            ["formulario" => $form->createView()]);
    }

    /**
     * @Route("/maleteo/deleteguardian/{id}", name="deleteguardian")
     */
    public function deleteguardian(Guardianes $guard, Request $request, EntityManagerInterface $em){

        $em->remove($guard);
        $em->flush();
        return $this->redirectToRoute("listguardianes") ;
    }


}